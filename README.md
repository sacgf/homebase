# Automation for managing sequencing data

Basecalling, QC, managing files and processes can become time consuming when you're doing multiple sequencing runs a week.

This project is designed to help automate this process

# SampleSheet.csv  

We've found that the SampleSheet.csv becomes the main handing off point from lab work to bioinformaticians - so it is useful to add extra columns to pass on more information, allowing us to:

* Generate scripts to perform basecalling
* Rename and move files to shared storage with per-group permissions
* Run QC or other programs on files (and perform summaries across projects)
* Raise issues, updating status as jobs complete, and assign to the appropriate people, so everyone stays informed

# Projects

Samples from the sequencing run are collected into projects (used for data directories and issues), which are named like *160715ProjectName*

ProjectName comes from the SampleSheet.csv Sample_Project column, or if not specified "ExperimentID", or failing that, the flowcell ID. NextSeq and MiSeq runs have the _Nextseq or _Miseq suffix. 

# Usage

Create a file called 'homebase.cfg' to fill in the defaults you need to supply. My copy looks like:

```
[defaults]
adapters=/data/sacgf/reference/adapters/fastqc_adapters.txt
barcodes=/data/sacgf/reference/barcodes/130CustomIlluminaBarcodes.csv
data_prefix=/data/sacgf
bcl2fastq_command=/data/sacgf/admin/tools/basecalling/bcl2fastq/bin/bcl2fastq
basecalling_base_dir=/home/dlawrence/scratch/basecalling
queue=queue
basecalling_cores=16
basecalling_walltime=99
issues_provider=bitbucket
issues_repo_owner=sacgf
issues_user=hiseq2000
issues_pass=put_your_password_here
issues_user_alias_csv=/home/users/XXXXXXXX/localwork/bioinformatics/config/sacgf/bioinformaticians.csv
issues_default_user=sacgf
```

You then run the seq_run.py, passing the config file and other parameters:

```
seq_run.py -c ~/homebase.cfg --email=me@email.com --fc-dir /data/ands_gdc/sequencers/hiseq2000/Runs/FCA/130715_SN1101_0107_AC2BDGACXX
```

This generates scripts to perform the steps, which you can submit (with dependencies) to PBS by running the generated "submit_jobs.sh" scripts 

# Install

Install a local copy of automation scripts

```
git clone https://bitbucket.org/sacgf/homebase
HOMEBASE_DIR=`pwd`/homebase
# Install Homebase dependencies
sudo pip install -r ${HOMEBASE_DIR}/requirements.txt
```

We require "homebase" to be in Python path, so before running (or added to .bashrc)

```
export PATH=${PATH}:${HOMEBASE_DIR}/bin
export PYTHONPATH=$PYTHONPATH:$LIBRARY_DIR:$HOMEBASE_DIR
```

# TODO:

A column for which default pipeline to run (RNAseq, Mir count, Variant Calling etc)