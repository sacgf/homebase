#!/usr/bin/env python
'''
Created on 10/12/2014

@author: dlawrence
'''

from Cheetah.Template import Template
from argparse import ArgumentParser
import os

from homebase import utils
from homebase.fastqc_parser import load_fastqc_data_for_dir

TOP_IMAGES = ["per_tile_quality.png"]
# values: (image_name, on_by_default)
IMAGES = [("per_base_quality.png", True),
          ("per_tile_quality.png", False),
          ("per_sequence_quality.png", False),
          ("per_base_sequence_content.png", True),
          ("per_sequence_gc_content.png", True),
          ("per_base_n_content.png", False),
          ("sequence_length_distribution.png", False),
          ("duplication_levels.png", False),
          ("adapter_content.png", False),
          ("kmer_profiles.png", False),
]
FASTQC_STATS_COLUMS = [('Encoding', False),
                       ('Sequence length', True),
                       ('Total Sequences', True),
                       ('GC', True)]

REPO_ROOT_DIR = utils.repo_dirname()
TEMPLATES_DIR = os.path.join(REPO_ROOT_DIR, "templates/reports")

def handle_args():
    # Args
    parser = ArgumentParser(description='FastQC Graph Summariser')
    parser.add_argument('--image-scale', help='Image scale percentage (default=100)', default=100)
    parser.add_argument('--image-list', help='comma sep list of image numbers to use (eg, 1-10 OR 1,4,7 OR 1-4,6,8-10) (default=1-10)', default="1-10")
    parser.add_argument('fastqc_base_dir', metavar='fastqc_dir', help='Path to directory of unzipped fastqc folders. Type \'image_item_list\' instead to see available images and corresponding image numbers')
    return parser.parse_args()

def generate_fastqc_graph_summary(args):
    fastqc_stats = load_fastqc_data_for_dir(args.fastqc_base_dir)
    template_file = os.path.join(TEMPLATES_DIR, "fastqc_graph_summary.tmpl")

    jquery_filename = os.path.join(REPO_ROOT_DIR, "resources", "lib", "js", "jquery-1.11.1.min.js")
    javascript = open(jquery_filename).read()
    name_space = {"fastqc_stats" : fastqc_stats,
                  'fastqc_stats_columns' : FASTQC_STATS_COLUMS,
                  "images" : IMAGES,
                  "image_scale" : args.image_scale,
                  "javascript" : javascript}

    if TOP_IMAGES and fastqc_stats:
        any_fastqc_dir = iter(fastqc_stats).next()
        top_images_full_path = [os.path.join(any_fastqc_dir, "Images", i) for i in TOP_IMAGES]
        name_space["top_images_full_path"] = top_images_full_path


    template = Template(file=template_file, searchList=[name_space])
    report_file_name = os.path.join(args.fastqc_base_dir, "fastqc_summary_tables.html")
    with open(report_file_name, "w") as f:
        f.write(str(template))    
    

if __name__ == '__main__':
    args = handle_args()
    generate_fastqc_graph_summary(args)
