#!/bin/bash

set -e

for script in $@; do
	if [ -z ${job_id} ]; then
		job_id=$(qsub ${script});
	else
	    job_id=$(qsub -W depend=afterok:${job_id} ${script})
	fi
	echo $job_id
done