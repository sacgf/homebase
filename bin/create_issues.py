#!/usr/bin/env python
'''
ACRF Cancer Genomics Facility

Created on 21/07/2016

@author: dlawrence
'''
from Cheetah.Template import Template
from argparse import ArgumentParser
import os

from homebase import utils
from homebase.illumina import samplesheet
from homebase.illumina.samplesheet import load_sample_sheet_as_sections, \
    get_trimming_info, load_old_sample_sheet, SAMPLE_NUMBER_COLUMN, \
    get_sample_numbers
from homebase.sequencing_run_projects import get_sequencing_run_projects, \
    do_lane_splitting
from python_bitbucket import api as bitbucket_api


def handle_args():
    parser = ArgumentParser(description='Update Sequencing Automation issues')
    parser.add_argument('--issues-repo-owner', required=True)
    parser.add_argument('--issues-user', required=True)
    parser.add_argument('--issues-pass', required=True)
    parser.add_argument('--issues-default-user', required=True)
    parser.add_argument('--issues-user-alias-csv', required=True)
    parser.add_argument('--lane-splitting', dest='lane_splitting', action='store_true')
    parser.add_argument('--data-prefix', required=True)
    parser.add_argument('--samplesheet', required=True)
    parser.add_argument('--fc-dir', required=True)
    parser.add_argument('--basecalling-dir', required=True)
    parser.add_argument('--email', required=True)
    parser.add_argument('--pbs-job-id')
    return parser.parse_args()


def get_issues_api(issues_user, issues_pass):
    proxy = utils.get_proxy_from_environment()
    api = bitbucket_api.API(issues_user, issues_pass, proxy=proxy)
    return api


def raise_issue(api, repo_owner, username, group, issue_title, issue_content):
    repo = api.get_repository(repo_owner, group)
    issue = repo.get_issue_by_title(issue_title)
    if issue:
        print "Issue '%s' exists - SKIPPING" % issue_title
    else:
        print "No existing issue by name '%s' - CREATING" % issue_title
        issue = repo.new_issue()
        issue.responsible = username
        issue.title = issue_title
        issue.kind = "task"
        issue.content = issue_content
        issue.save()


def raise_issues_for_sequencing_run_projects(api, email, issues_repo_owner, basecalling_job_id, sequencing_run_projects, trimming_message):    
    for proj in sequencing_run_projects:
        samples = []
        samples_set = set()
        for s in proj.sample_descriptions:
            sample_string = str(s)
            if sample_string not in samples_set:
                samples.append(s)
                samples_set.add(sample_string)
                
        name_space = {'project_dir' : proj.project_dir,
                      'group' : proj.group,
                      'samples' : samples,
                      'trimming_message' : trimming_message,
                      'basecalling_details' : {"job_id" : basecalling_job_id, "email" : email}
        }

        templates_dir = os.path.join(utils.templates_dirname(), "issue_templates", "bitbucket")
        if proj.unaligned_project_dir:
            extra_params = {"unaligned_project_dir" : proj.unaligned_project_dir}

            template_file = os.path.join(templates_dir, "issue_auto_copy.tmpl")
            template = Template(file=template_file, searchList=[name_space, extra_params])
            issue_content = str(template)
        else:
            extra_params = {"fastq_dir" : proj.basecalling_proj_dir}
            template_file = os.path.join(templates_dir, "issue_manual_copy.tmpl")
            template = Template(file=template_file, searchList=[name_space, extra_params])
            issue_content = str(template)   

        raise_issue(api, issues_repo_owner, proj.username, proj.group, proj.project_dir, issue_content)


def main():
    args = handle_args()
    api = get_issues_api(args.issues_user, args.issues_pass)


    if samplesheet.is_new_sheet(args.samplesheet):
        sample_sheet_sections = load_sample_sheet_as_sections(args.samplesheet)
        settings_section = sample_sheet_sections["Settings"]
        _, trimming_message = get_trimming_info(settings_section)
        sample_sheet_df = sample_sheet_sections["Data"]
    else:
        sample_sheet_df = load_old_sample_sheet(args.samplesheet)
        trimming_message = "No trimming will occur as SampleSheet.csv is old format and missing Settings section (and adapters)."

    (run_date, fcid) = samplesheet.get_run_date_and_fcid_from_dir(args.fc_dir)

    lane_splitting = do_lane_splitting(args)
    sample_sheet_df[SAMPLE_NUMBER_COLUMN] = get_sample_numbers(sample_sheet_df, lane_splitting)

    sequencing_run_projects = get_sequencing_run_projects(args, args.basecalling_dir, run_date, fcid, lane_splitting, sample_sheet_df)
    raise_issues_for_sequencing_run_projects(api, args.email, args.issues_repo_owner, args.pbs_job_id, sequencing_run_projects, trimming_message)
    

if __name__ == '__main__':
    main()
