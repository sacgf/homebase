#!/usr/bin/env python
'''
Created on 4Nov.,2016

@author: dlawrence
'''

from matplotlib import pyplot
from argparse import ArgumentParser
from illuminate.index_metrics import InteropIndexMetrics
from illuminate.tile_metrics import InteropTileMetrics
import seaborn as sns


def handle_args():
    parser = ArgumentParser(description='Display graphs and write CSV from Illumina basecalling')
    parser.add_argument('--tile-metrics', required=True, help="TileMetricsOut.bin")
    parser.add_argument('--index-metrics', required=True, help="IndexMetricsOut.bin")
    return parser.parse_args()

def write_csv_and_plot(read_1_df, name, num_undetermined):
    a = sns.color_palette()[2]
    b = sns.color_palette()[0]
    index_totals = read_1_df.groupby(name)["clusters"].sum()
    index_totals["Undetermined"] = num_undetermined
    c = [a if i == 'Undetermined' else b for i in index_totals.index]
    pyplot.figure(figsize=(20, 13.75))
    index_totals.plot.bar(color=c)
    pyplot.savefig("barcodes_by_%s.png" % name)#, dpi=1000)
    index_totals.to_csv("barcodes_by_%s.csv" % name)

def main(args):
    tm = InteropTileMetrics(args.tile_metrics)
    iim = InteropIndexMetrics(args.index_metrics)
    read_1_mask = iim.df["read"] == 1
    read_1_df = iim.df[read_1_mask]
    
    num_clusters_pf = tm.num_clusters_pf
    num_indexed_pf = read_1_df["clusters"].sum()
    num_undetermined = num_clusters_pf - num_indexed_pf  
    
    for name in ["index", "name"]:
        name += "_str"
        write_csv_and_plot(read_1_df, name, num_undetermined)


if __name__ == '__main__':
    args = handle_args()
    main(args)