#!/usr/bin/env python
'''

This looks in the top unknown barcodes (from Illumina basecalling report) and sees if it contains any known barcodes

This could indicate sample mixups

Created on 4Nov.,2016

@author: dlawrence
'''
from collections import defaultdict
from argparse import ArgumentParser
import bs4
from itertools import izip_longest
import os

import pandas as pd

BARCODES_MESSAGE = "barcodes_message.txt"
BC_INDEX_COL = "ID"
BC_BARCODE_COL = "Barcode (8bp)"


def handle_args():
    parser = ArgumentParser(description='Looks for indexes in top unknown barcodes')
    parser.add_argument('--report-dir', required=True, help="Basecalling Report Dir")
    parser.add_argument('barcode_csv', nargs='+', help='File of barcodes (ID, Barcode (8bp))')
    return parser.parse_args()

def get_subdirs(d):
    return [os.path.join(d,o) for o in os.listdir(d) if os.path.isdir(os.path.join(d,o))]

def checkdir(d):
    if not os.path.isdir(d):
        msg = "'%s' is not a directory!" % d
        raise ValueError(msg)

#https://docs.python.org/2/library/itertools.html#recipes
def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return izip_longest(*args, fillvalue=fillvalue)

# file:///data/sacgf/molpath/data/unaligned/161027HamishScott_Nextseq/basecalling/Reports/html/HGGLCAFXX/default/Undetermined/unknown/lane.html
def get_unknown_barcodes_file(report_dir):
    checkdir(report_dir)
    html_dir = os.path.join(report_dir, 'html')
    checkdir(html_dir)
    subdirs = get_subdirs(html_dir)
    num_subdirs = len(subdirs)
    if num_subdirs != 1:
        msg = "Expected exactly 1 subdir of '%s' (got %d)" % (html_dir, num_subdirs)
        raise ValueError(msg)
    
    flowcell_dir = subdirs[0]
    return os.path.join(flowcell_dir, "default", "Undetermined", "unknown", "lane.html")

def get_top_unknown_barcode_occurances_by_lane(unknown_barcodes_file):
    soup = bs4.BeautifulSoup(open(unknown_barcodes_file), "lxml")
    last_table = soup.find_all('table')[-1]
    rows = last_table.find_all('tr')

    # Sanity check...
    headers = rows[0].find_all('th')
    expected = ["Lane", "Count", "Sequence"]
    for i, triplet in enumerate(grouper(headers, 3)):
        actual = [(x or '').get_text().strip() for x in triplet]
        if actual != expected:
            msg = 'Expected headers to be %s, was: %s' % (expected, triplet)
            raise ValueError(msg)
    #num_lanes = i + 1
    #print "There are %d lanes" % num_lanes
    top_unknown_barcode_occurances_by_lane = defaultdict(dict)
        
    for row in rows[1:]:
        for i, pair in enumerate(grouper(row.find_all('td'), 2)):
            lane = i+1
            (count_str, sequence) = [x.get_text().strip() for x in pair]
            count = int(count_str.replace(',', ''))
            #print "%d %s: %d" % (lane, sequence, count)

            top_unknown_barcode_occurances_by_lane[sequence][lane] = count

    return top_unknown_barcode_occurances_by_lane


def main(args):
    unknown_barcodes_file = get_unknown_barcodes_file(args.report_dir)
    top_unknown_barcode_occurances_by_lane = get_top_unknown_barcode_occurances_by_lane(unknown_barcodes_file)

    known_barcodes = []
    num_checked_barcodes = 0
    for bc_csv in args.barcode_csv:
        bc_df = pd.read_csv(bc_csv, sep=',', index_col=BC_INDEX_COL)
        barcodes = bc_df[BC_BARCODE_COL]
        num_checked_barcodes += len(barcodes)

        sequence_labels = {}
        for (bc_label, bc_seq) in barcodes.iteritems():
            sequence_labels[bc_seq] = bc_label
 
        for sequence, occurances_by_lane in top_unknown_barcode_occurances_by_lane.iteritems():
            sequence_label = sequence_labels.get(sequence)
            if sequence_label:
                occurances = [] 
                for (lane, num_occ) in sorted(occurances_by_lane.iteritems()):
                    occurances.append("L%d: %d occ" % (lane, num_occ))
                occurances_str = ",".join(occurances)
                warning = "Undetermined barcode '%s' is known barcode %s from %s. Occurs: %s" % (sequence, bc_label, bc_csv, occurances_str)
                known_barcodes.append(warning)


    output_file = os.path.join(args.report_dir, BARCODES_MESSAGE)
    with open(output_file, "w") as f:
        # TODO: Write out what % the unknown barcodes are.
        f.write("Checked Top Unknown barcodes against %d known barcodes.\n" % num_checked_barcodes)

        if known_barcodes:
            for kb in known_barcodes:
                f.write(kb + "\n")

if __name__ == '__main__':
    args = handle_args()
    main(args)