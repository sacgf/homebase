#!/usr/bin/env python
'''
ACRF Cancer Genomics Facility

Created on 31/07/2013

@author: dlawrence
'''

from Cheetah.Template import Template
from argparse import ArgumentParser
import logging
import os
import stat

from homebase import utils
from homebase.default_config_argparse import parse_args_with_config_default
from homebase.illumina import samplesheet
from homebase.illumina.samplesheet import load_sample_sheet_as_sections, \
    get_trimming_info, load_old_sample_sheet, get_sample_numbers, \
    SAMPLE_NUMBER_COLUMN
from homebase.sequencing_run_projects import get_sequencing_run_projects, \
    do_lane_splitting


REPO_ROOT_DIR = utils.repo_dirname()
TEMPLATES_DIR = utils.templates_dirname()
BASECALLING_MEM = 30

def handle_args():
    DEFAULT_SAMPLESHEET = "SampleSheet.csv"
    
    parser = ArgumentParser(description='Automate Basecalling pipeline')
    parser.add_argument('--adapters', help='Adapters passed to FastQC', required=False)
    parser.add_argument('--barcodes', help='Check Undetermined barcodes for sequences from these files (comma sep)', required=False)
    parser.add_argument('--data-prefix')
    parser.add_argument('--basecalling-base-dir')
    parser.add_argument('--bcl2fastq-command')
    parser.add_argument('--allow-bcl2fastq-trimming', help="Don't throw an error if adapters are in the SampleSheet.csv", action='store_true')
    parser.add_argument('--lane-splitting', dest='lane_splitting', action='store_true')
    parser.add_argument('--basecalling-cores')
    parser.add_argument('--basecalling-walltime')
    parser.add_argument('--issues-repo-owner')
    parser.add_argument('--issues-user')
    parser.add_argument('--issues-pass')
    parser.add_argument('--issues-default-user')
    parser.add_argument('--issues-user-alias-csv')
    parser.add_argument('--group',          help='Created directories are assigned to this group')
    parser.add_argument('--queue',          help="PBS queue")
    parser.add_argument('--cores',          help='Basecalling cores')
    parser.add_argument('--email',          required=True)
    parser.add_argument('--fc-dir',          required=True, help="Flow Cell Dir")
    parser.add_argument('--log-level',      default=logging.INFO)
    parser.add_argument('--samplesheet',    help="SampleSheet (defaults to ${fcdir}/%s)" % DEFAULT_SAMPLESHEET)

    args = parse_args_with_config_default(parser)
    if args.samplesheet is None:
        args.samplesheet = os.path.join(args.fc_dir, DEFAULT_SAMPLESHEET)

    args.stripped_samplesheet_dir = os.path.join(args.basecalling_base_dir, "stripped_samplesheets")
    
    return args
    
def use_pbs_modules():
    return True


def get_interop_dir(runfolder_dir, basecalling_dir):
    ''' Use runfolder/InterOp (default) if we can write, otherwise basecalling dir/InterOp '''
    interop_dir = os.path.join(runfolder_dir, "InterOp")
    if not os.access(interop_dir, os.W_OK):
        interop_dir = os.path.join(basecalling_dir, "InterOp")
    return interop_dir


def generate_basecalling_script(config, lane_splitting, interop_dir, work_dir, fcid, sample_sheet_filename):
    name = "basecalling_%s" % fcid
    cores = config.basecalling_cores

    env = {"WORKDIR" : work_dir,
           "CORES" : cores,
           "BCL2FASTQ" : config.bcl2fastq_command,
           "SAMPLE_SHEET" : sample_sheet_filename,
           "RUN_FOLDER_DIR" : config.fc_dir,
           "INTEROP_DIR" : interop_dir}

    commands = []
    if use_pbs_modules():
        commands.append("module load gnu/6.2.0 zlib/1.2.8-gnu_4.8.0 bcl2fastq/2.18.0.12") # Most recent fastQC

    bcl_cmd = "bcl2fastq --processing-threads=${CORES} --runfolder-dir=${RUN_FOLDER_DIR} --sample-sheet=${SAMPLE_SHEET} --output-dir=${WORKDIR} --interop-dir=${INTEROP_DIR}"
    if not lane_splitting:
        commands.append("# The --no-lane-splitting option merges the FastQs from different lanes together (NextSeq has 4 lanes 1 reservoir)")
        bcl_cmd += " --no-lane-splitting "
    commands.append(bcl_cmd)
    
    # Internal basecalling QC
    commands.append("env_runner.sh barcode_graphs.py --tile-metrics=${RUN_FOLDER_DIR}/InterOp/TileMetricsOut.bin --index-metrics=${INTEROP_DIR}/IndexMetricsOut.bin")

    if config.barcodes:
        barcodes = " ".join(config.barcodes.split(','))
        commands.append('env_runner.sh undetermined_barcode_check.py --report-dir="${WORKDIR}/Reports" %s' % barcodes)
    
    name_space = {"name" : name,
                  "email" : config.email,
                  "cores" : cores,
                  "mem" : BASECALLING_MEM,
                  "vmem" : BASECALLING_MEM,
                  "walltime" : config.basecalling_walltime,
                  "workdir" : work_dir,
                  "commands" : commands,
                  "queue" : config.queue,
		          "env" : env,
                  "path" : homebase_bin_dir,
    }

    single_file_script_template = os.path.join(TEMPLATES_DIR, "script_templates/single_file_script.tmpl")
    template = Template(file=single_file_script_template, searchList=[name_space])
    out_script = os.path.join(scripts_dir, "%s.csh" % name)
    utils.mk_path_for_file(out_script)
    with open(out_script, "w") as f:
        f.write(str(template))
    return out_script


# TODO: This name is bad, maybe it's the housekeeping script
def generate_housekeeping_script(config, homebase_bin_dir, interop_dir, basecalling_dir, scripts_dir, fcid, sequencing_run_projects):
    name = "housekeeping_%s" % fcid
    name_space = {  "name" : name,
                    "email" : config.email,
                    "sequencing_run_projects" : sequencing_run_projects,
                    "queue" : config.queue,
                    "sample_sheet" : config.samplesheet,
                    "interop_dir" : interop_dir,
                    "path" : homebase_bin_dir,
                    "issues_repo_owner" : config.issues_repo_owner,
                    "issues_user" : config.issues_user,
                    "issues_pass" : config.issues_pass,
                    "basecalling_dir" : basecalling_dir,
                    "group" : config.group,
    }

    template_file = os.path.join(TEMPLATES_DIR, "script_templates", "housekeeping.tmpl")
    template = Template(file=template_file, searchList=[name_space])
    out_script = os.path.join(scripts_dir, "%s.csh" % name)
    utils.mk_path_for_file(out_script)
    with open(out_script, "w") as f:
        f.write(str(template))
    return out_script


def generate_qc_script(config, homebase_bin_dir, scripts_dir, fcid, sequencing_run_projects):
    name = "qc_%s" % fcid
    name_space = {  "name" : name,
                    "email" : config.email,
                    "sequencing_run_projects" : sequencing_run_projects,
                    "queue" : config.queue,
                    "path" : homebase_bin_dir,
                    "issues_repo_owner" : config.issues_repo_owner,
                    "issues_user" : config.issues_user,
                    "issues_pass" : config.issues_pass,
                    "group" : config.group,
                    "adapters" : config.adapters,
    }

    template_file = os.path.join(TEMPLATES_DIR, "script_templates", "qc_samples.tmpl")
    template = Template(file=template_file, searchList=[name_space])
    out_script = os.path.join(scripts_dir, "%s.csh" % name)
    utils.mk_path_for_file(out_script)
    with open(out_script, "w") as f:
        f.write(str(template))
    return out_script


def name_from_file_name(file_name):
    '''Gets file name with removing extension and directory'''
    return os.path.splitext(os.path.basename(file_name))[0]


def create_launch_script(config, repo_root_dir, homebase_bin_dir, scripts_dir, basecalling_dir, dependent_scripts):
    launch_script = os.path.join(scripts_dir, "submit_jobs.sh")
    initial_script_variable = None

    with open(launch_script, "w") as f:
        f.write("#!/bin/bash\n\n")
        f.write("export PYTHONPATH=${PYTHONPATH}:%s\n\n" % repo_root_dir)
        f.write("export PATH=${PATH}:%s\n\n" % homebase_bin_dir)
        
        dependency = None
        for script in dependent_scripts:
            script_variable = name_from_file_name(script).upper()
            if initial_script_variable is None:
                initial_script_variable = script_variable

            params = {"dependency" : dependency,
                      "script" : script}

            if dependency:
                qsub = "qsub -W depend=afterok:${%(dependency)s} %(script)s" % params  
            else:
                qsub = "qsub %(script)s" % params

            line = "%(script_variable)s=$(%(qsub)s)\n" % {"script_variable" : script_variable, "qsub" : qsub}
            f.write(line)
            dependency = script_variable
            

        params = {"create_issues_script" : "create_issues.py",
                  "issues_repo_owner" : config.issues_repo_owner,
                  "issues_user" : config.issues_user,
                  "issues_pass" : config.issues_pass,
                  "issues_default_user" : config.issues_default_user,
                  "issues_user_alias_csv" : config.issues_user_alias_csv,
                  "data_prefix" : config.data_prefix,
                  "fc_dir" : config.fc_dir,
                  "basecalling_dir" : basecalling_dir,
                  "email" : config.email,
                  "samplesheet" : config.samplesheet,
                  "initial_script_variable" : initial_script_variable,}

        if config.lane_splitting:
            params["lane_splitting"] = "--lane-splitting"
        else:
            params["lane_splitting"] = ""

        issues_command_pattern = """
env_runner.sh %(create_issues_script)s \
--issues-repo-owner=%(issues_repo_owner)s --issues-user=%(issues_user)s --issues-pass=%(issues_pass)s \
--issues-default-user=%(issues_default_user)s --issues-user-alias-csv=%(issues_user_alias_csv)s %(lane_splitting)s \
--data-prefix=%(data_prefix)s --fc-dir=%(fc_dir)s --basecalling-dir=%(basecalling_dir)s --samplesheet=%(samplesheet)s --email=%(email)s \
--pbs-job-id=${%(initial_script_variable)s}\n"""
        f.write("\n")
        f.write(issues_command_pattern % params)

    st = os.stat(launch_script)
    os.chmod(launch_script, st.st_mode | stat.S_IEXEC)

    return launch_script

if __name__ == '__main__':
    config = handle_args()
    
    name = os.path.basename(__file__)
    log_format = "%(levelname)s:%(asctime)s:%(name)s:%(message)s"
    logger = utils.setup_console_logger(name=name, log_level=config.log_level, log_format=log_format)

    logger.debug("Using samplesheet '%s'" % config.samplesheet)

    if samplesheet.is_new_sheet(config.samplesheet):
        sample_sheet_sections = load_sample_sheet_as_sections(config.samplesheet)
        
        settings_section = sample_sheet_sections["Settings"]
        trimming, trimming_message = get_trimming_info(settings_section)
        if trimming and not config.allow_bcl2fastq_trimming:
            msg = "We don't allow by default SampleSheet.csv settings: %s. Add the --allow-bcl2fastq-trimming option to override" % trimming_message
            raise ValueError(msg) 
        print trimming_message
            
        sample_sheet_df = sample_sheet_sections["Data"]
    else:
        sample_sheet_df = load_old_sample_sheet(config.samplesheet)
    
    (run_date, fcid) = samplesheet.get_run_date_and_fcid_from_dir(config.fc_dir)

    basecalling_dir = os.path.join(config.basecalling_base_dir, "basecalling_%(fcid)s" % {"fcid" : fcid})
    scripts_dir = os.path.join(basecalling_dir, "scripts")
    homebase_bin_dir = os.path.dirname(__file__)

    lane_splitting = do_lane_splitting(config)
    sample_sheet_df[SAMPLE_NUMBER_COLUMN] = get_sample_numbers(sample_sheet_df, lane_splitting)
    
    interop_dir = get_interop_dir(config.fc_dir, basecalling_dir)

    sequencing_run_projects = get_sequencing_run_projects(config, basecalling_dir, run_date, fcid, lane_splitting, sample_sheet_df)

    basecalling_script = generate_basecalling_script(config, lane_splitting, interop_dir, basecalling_dir, fcid, config.samplesheet)
    housekeeping_script = generate_housekeeping_script(config, homebase_bin_dir, interop_dir, basecalling_dir, scripts_dir, fcid, sequencing_run_projects)
    qc_script = generate_qc_script(config, homebase_bin_dir, scripts_dir, fcid, sequencing_run_projects)

    scripts = [basecalling_script, housekeeping_script, qc_script]
    launch_script = create_launch_script(config, REPO_ROOT_DIR, homebase_bin_dir, scripts_dir, basecalling_dir, scripts)

    print "No jobs submitted - launch script: "
    print launch_script


