#!/bin/bash

# Initialise whatever you need to do, eg I use:
BASH_MODULE_INIT=/opt/shared/system/Modules/3.2.7/init/bash
if [[ -e ${BASH_MODULE_INIT} ]]; then
	echo "Loading module..."
	. ${BASH_MODULE_INIT}
	module load python/4.4.3/2.7.2
	source /data/sacgf/admin/python_virtual_environments/sacgf/bin/activate
fi

HOMEBASE_DIR=$(dirname $0)/..
export PYTHONPATH=${PYTHONPATH}:${HOMEBASE_DIR}
echo "Pythonpath = ${PYTHONPATH}"

exec "$@"

