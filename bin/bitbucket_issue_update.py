#!/usr/bin/env python
'''
ACRF Cancer Genomics Facility

Created on 11/09/2013

@author: dlawrence
'''

from argparse import ArgumentParser
from homebase import utils
from python_bitbucket import api as bitbucket_api
import logging
import re

FASTQC_HEADER = "## FastQC:  not done  ##"
FASTQC_MESSAGE = "A job will be launched, and the issue updated upon completion."

def handle_args():
    parser = ArgumentParser(description='Update Sequencing Automation issues')
    parser.add_argument('--group',          required=True, help="immun / molpath / womchild etc")
    parser.add_argument('--issue-title',    required=True, help="Issue title")
    parser.add_argument('--section',        help="(optional) Issue section (FastQC, Copy to shared storage")
    parser.add_argument('--message')
    parser.add_argument('--issues-repo-owner', required=True)
    parser.add_argument('--issues-user', required=True)
    parser.add_argument('--issues-pass', required=True)
    parser.add_argument('--verbose', action='store_true')
    return parser.parse_args()

def replace_section(logger, content, section, message):
    ''' If you pass section, look for "section: not done" then replace with done
        Otherwise, just append at the end
    '''
    SECTION_HEADER_REGEX = re.compile("^##\s+%s.*not done" % section)

    in_section = False
    replaced_section = False
    lines = []
    for line in content.split("\n"):
        if section:
            if in_section:
                if line.startswith("#"): # End of section - replace
                    lines.append("## %s: done ##" % section)
                    if message:
                        lines.append("")
                        lines.append(message)
                    in_section = False
                    replaced_section = True
            elif SECTION_HEADER_REGEX.match(line):
                in_section = True    

        if not in_section:
            lines.append(line)

    if not section or not replaced_section:
        if section:
            logger.debug("section '%s' wasn't found, creating" % section)
            lines.append("## %s: ##" % section)
        else:
            lines.append("")
        if message:
            lines.append("")
            lines.append(message)

    return "\n".join(lines)

def get_repo(args):
    proxy = utils.get_proxy_from_environment()
    logger.debug("Using proxy of %s" % proxy)
    api = bitbucket_api.API(args.issues_user, args.issues_pass, proxy=proxy)
    return api.get_repository('sacgf', args.group)

args = handle_args()

if args.verbose:
    log_level = logging.DEBUG
else:
    log_level = logging.INFO

logger = utils.setup_console_logger(log_level=log_level)

repo = get_repo(args)
issue = repo.get_issue_by_title(args.issue_title)
if issue is None:
    raise ValueError("Could not find issue %s in repo %s" % (args.issue_title, repo))

new_content = replace_section(logger, issue.content, args.section, args.message)
if new_content != issue.content:
    logger.info("Content changed, updating issue...")
    logger.debug("%s" % new_content)
    issue.content = new_content
    issue.update()
else:
    logger.info("No changes made to issue...")
