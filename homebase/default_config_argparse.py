import ConfigParser
import argparse
import os

def parse_args_with_config_default(parser):
    # Parse any conf_file specification
    # We make this parser with add_help=False so that
    # it doesn't parse -h and print help.
    conf_parser = argparse.ArgumentParser(
        description=__doc__, # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
        )

    # Do for both parsers
    def add_config_file_option(p):
        p.add_argument("-c", "--conf_file",
                        help="Specify config file", metavar="FILE")
    
    add_config_file_option(parser)
    add_config_file_option(conf_parser)

    args, remaining_argv = conf_parser.parse_known_args()
    if args.conf_file:
        if os.path.exists(args.conf_file):
            print "Reading config file '%s'" % args.conf_file
        else:
            msg = "Config file '%s' does not exist" % args.conf_file
            raise ValueError(msg)

        config = ConfigParser.SafeConfigParser()
        config.read([args.conf_file])
        defaults = dict(config.items("defaults"))
    else:
        defaults = {}

    parser.set_defaults(**defaults)
    return parser.parse_args(remaining_argv)
