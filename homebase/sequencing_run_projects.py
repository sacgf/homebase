'''
Created on 21/07/2016

@author: dlawrence
'''

import logging
import os

from homebase.illumina.run_parameters import IlluminaSequencer, \
    get_run_parameters
from homebase.illumina.samplesheet import SAMPLE_ID_COLUMN, GROUP_COLUMN, \
    SAMPLE_PROJECT_COLUMN, DESCRIPTION_COLUMN, LANE_COLUMN, \
    BIOINFORMATICIAN_COLUMN, RECIPE_COLUMN, SAMPLE_NUMBER_COLUMN
import pandas as pd


SEQUENCER_DEFAULT_SUFFIX = {IlluminaSequencer.MISEQ : '_Miseq',
                            IlluminaSequencer.NEXTSEQ : '_Nextseq',
}


class SequencingRunProject(object):
    def __init__(self, data_prefix, basecalling_dir, sample_project, group, project_dir, samples_numbers_and_lanes, sample_descriptions, username, paired_end):
        self.data_prefix = data_prefix
        self.basecalling_dir = basecalling_dir
        self.group = group
        self.project_dir = project_dir
        self.samples_numbers_and_lanes = samples_numbers_and_lanes
        self.sample_descriptions = sample_descriptions
        self.username = username
        self.unaligned_project_dir = None
        self.sample_project = sample_project
        self.paired_end = paired_end

        name_space = {'project_dir' : self.project_dir,
                      'group' : self.group,
        }
        
        base_dir = os.path.join(self.data_prefix, self.group)
        if not os.path.isdir(base_dir):
            msg = "Warning: group directory '%s' does not exist! Create it (or your group is wrong?)" % base_dir
            raise ValueError(msg)
        
        dir_pattern = os.path.join(self.data_prefix, "%(group)s", "data", "unaligned", "%(project_dir)s")
        self.unaligned_project_dir = dir_pattern % name_space

        self.fastq_src_dest = self.get_fastq_src_dest()
        

    def get_fastq_src_dest(self):
        ''' tuple of (src, dest) '''
        
        SRC_FASTQ_PATTERN = "%(sample_name)s_S%(sample_number)d%(lane_section)s_R%(read)d_001.fastq.gz"
        DEST_FASTQ_PATTERN = "%(sample_name)s_R%(read)d.fastq.gz"
        
        src_fastq_dir = os.path.join(self.basecalling_dir)
        if self.sample_project:
            src_fastq_dir = os.path.join(src_fastq_dir, self.sample_project)
        
        dest_fastq_dir = os.path.join(self.unaligned_project_dir, "fastq")
        
        reads = [1]
        if self.paired_end:
            reads.append(2)

        unique_dest_fastqs = set()
        fastq_src_dest = []
        for _, samples_numbers_and_lanes in self.samples_numbers_and_lanes.iterrows():
            sample = samples_numbers_and_lanes[SAMPLE_ID_COLUMN]
            sample_number = samples_numbers_and_lanes[SAMPLE_NUMBER_COLUMN]
            lane = samples_numbers_and_lanes[LANE_COLUMN]
            if lane:
                lane_section = "_L%03d" % lane
            else:
                lane_section = ''
            for read in reads:
                params = {"sample_name" : sample,
                          "sample_number" : sample_number,
                          "read" : read,
                          "lane_section" : lane_section}

                src_fastq_name = SRC_FASTQ_PATTERN % params
                src_fastq = os.path.join(src_fastq_dir, src_fastq_name)

                dest_fastq_name = DEST_FASTQ_PATTERN % params
                # Do a sanity check to make sure we don't blow away files
                if dest_fastq_name in unique_dest_fastqs:
                    msg = "Destination FastQ after move would be '%s' which is not unique.\n" % dest_fastq_name
                    msg += "Original fastq name was: %s " % src_fastq_name
                    #logging.warn(msg)
                    continue # Skip as was merged...
                unique_dest_fastqs.add(dest_fastq_name)

                dest_fastq = os.path.join(dest_fastq_dir, dest_fastq_name)
                fastq_src_dest.append((src_fastq, dest_fastq))

        return fastq_src_dest


    def __str__(self):
        return "%s (%d samples)" % (self.project_dir, len(self.samples_numbers_and_lanes))


class UserSelector(object):
    def __init__(self, issues_default_user, issues_user_alias_csv):
        self.default_user = issues_default_user
        self.alias = {}

        self.issues_user_alias_csv = issues_user_alias_csv
        df = pd.DataFrame.from_csv(issues_user_alias_csv)
        for (alias, user) in df["bitbucket_username"].iteritems():
            self.alias[alias] = user
        
    
    #     
#    bioinformatician_df = pd.DataFrame.from_csv(config.issues_user_alias_csv)
    def get_user(self, sample_project, project_df):
        if BIOINFORMATICIAN_COLUMN in project_df:
            bioinformaticians = project_df[BIOINFORMATICIAN_COLUMN].dropna().unique()
            bioinformatician_name = None
            if not bioinformaticians:
                print "Project dir = %s had no assigned bioinformaticians" % sample_project
            elif len(bioinformaticians) > 1:
                print "Project dir %s has >1 bioinformaticians (%s)" % (sample_project, bioinformaticians)
            else:
                bioinformatician_name = bioinformaticians[0]

            bitbucket_user = self.alias.get(bioinformatician_name) # Use alias if available
            if bitbucket_user is None:
                logging.warn("No user '%s' has bitbucket user stored in alias file %s" % (bioinformatician_name, self.issues_user_alias_csv)) 
            return bitbucket_user or self.default_user


def do_lane_splitting(config):
    if config.lane_splitting:
        sequencer_model, _, _, _ = get_run_parameters(config.fc_dir)
        if sequencer_model == IlluminaSequencer.NEXTSEQ:
            print "Warning: --late-splitting set for NextSeq run which doesn't have real lanes"

    return config.lane_splitting



def get_sample_descriptions(df):
    samples = []
    for _, row in df.iterrows():
        recipe_data = row[RECIPE_COLUMN]
        if pd.isnull(recipe_data):
            recipe_str = ""
        else:
            recipe_str = ", %s" % recipe_data
        sample_details = "%s (%s%s)" % (row[SAMPLE_ID_COLUMN], row[DESCRIPTION_COLUMN], recipe_str)
        samples.append(sample_details)
    return samples

def get_samples_numbers_and_lanes(df, lane_splitting):
    if lane_splitting and LANE_COLUMN in df.columns:
        samples_numbers_and_lanes = df[[SAMPLE_ID_COLUMN, SAMPLE_NUMBER_COLUMN, LANE_COLUMN]]
    else:
        samples_numbers_and_lanes = df[[SAMPLE_ID_COLUMN, SAMPLE_NUMBER_COLUMN]].assign(**{LANE_COLUMN : lambda x : None})
    return samples_numbers_and_lanes


def get_multiple_sequencing_run_projects_for_run(config, basecalling_dir, paired_end, run_date, project_suffix, lane_splitting, sample_sheet_df):
    ''' Samplesheet contains multiple Sample_Project entries '''
    user_selector = UserSelector(config.issues_default_user, config.issues_user_alias_csv)

    sequencing_run_projects = []
    for ((group, sample_project), df) in sample_sheet_df.groupby([GROUP_COLUMN, SAMPLE_PROJECT_COLUMN]):
        project_dir = "%s%s%s" % (run_date, sample_project, project_suffix)

        samples_numbers_and_lanes = get_samples_numbers_and_lanes(df, lane_splitting)
        sample_descriptions = get_sample_descriptions(df)
        user = user_selector.get_user(sample_project, df)

        sequencing_run_project = SequencingRunProject(config.data_prefix, basecalling_dir, sample_project, group, project_dir, samples_numbers_and_lanes, sample_descriptions, user, paired_end)
        sequencing_run_projects.append(sequencing_run_project)
        
    return sequencing_run_projects


def get_single_project_for_run(config, basecalling_dir, paired_end, run_name, project_name, lane_splitting, sample_sheet_df):
    user_selector = UserSelector(config.issues_default_user, config.issues_user_alias_csv)
    user = user_selector.get_user(config.fc_dir, sample_sheet_df)
    groups = sample_sheet_df[GROUP_COLUMN].dropna().unique()
    if len(groups) != 1:
        msg = "Project is whole sequencing run, expecting exactly 1 group in '%s' column, was: %s" % (GROUP_COLUMN, groups)
        raise ValueError(msg)
    
    group = groups[0]
    samples_numbers_and_lanes = get_samples_numbers_and_lanes(sample_sheet_df, lane_splitting)
    sample_descriptions = get_sample_descriptions(sample_sheet_df)

    sequencing_run_project = SequencingRunProject(config.data_prefix, basecalling_dir, project_name, group, run_name, samples_numbers_and_lanes, sample_descriptions, user, paired_end)
    return sequencing_run_project


def get_sequencing_run_projects(config, basecalling_dir, run_date, fcid, lane_splitting, sample_sheet_df):
    sequencer_model, _, experiment_name, paired_end = get_run_parameters(config.fc_dir)
    project_suffix = SEQUENCER_DEFAULT_SUFFIX.get(sequencer_model, '')
    projects = sample_sheet_df[SAMPLE_PROJECT_COLUMN].dropna().unique()

    if len(projects) > 1:
        print "SampleSheet contains multiple projects"
        sequencing_run_projects = get_multiple_sequencing_run_projects_for_run(config, basecalling_dir, paired_end, run_date, project_suffix, lane_splitting, sample_sheet_df)
    else:
        project_name = None
        if len(projects) == 1:
            project_name = projects[0]
            run_id = project_name
        elif experiment_name:
            run_id = experiment_name
        else:
            print "Warning: Using whole run as project, but no %s from SampleSheet or ExperimentID (RunParameters.xml) (falling back on FCID)" % SAMPLE_PROJECT_COLUMN
            run_id = fcid
        run_name = "%s%s%s" % (run_date, run_id, project_suffix)

        print "Treating entire sequencing run as single project: %s" % run_name
        
        sequencing_run_project = get_single_project_for_run(config, basecalling_dir, paired_end, run_name, project_name, lane_splitting, sample_sheet_df)
        sequencing_run_projects = [sequencing_run_project]

    if not sequencing_run_projects:
        group_and_projects_df = sample_sheet_df[[GROUP_COLUMN, SAMPLE_PROJECT_COLUMN]]
        msg = "No sequencing run projects created!! Check group and project columns: %s" % group_and_projects_df
        raise ValueError(msg)

    return sequencing_run_projects
