'''
ACRF Cancer Genomics Facility

"Util" - the place for things where you can't think of a name...

@author: dlawrence
'''

from cStringIO import StringIO
import logging
import os
import pandas as pd
import sys

def dataframe_from_commented_csv(csv_filename, **kwargs):
    ''' Reads csv skipping files starting with '#' (or comment= kwarg) '''

    comment = kwargs.pop('comment', '#')
    s = StringIO()
    with open(csv_filename) as f:
        for line in f:
            if not line.startswith(comment):
                s.write(line)
    s.seek(0)
    return pd.read_csv(s)

def up_dir(path, n):
    ''' Basically like os.path.join(path, "../" * n) but without the dots '''
    assert n >= 0
    path_components = path.split(os.path.sep)
    return os.path.sep.join(path_components[:-n])

def repo_dirname():
    ''' Returns the full path name of the repo '''
    return up_dir(os.path.dirname(__file__), 1) # Relative to current file (change if you ever move!)

def templates_dirname():
    return os.path.join(repo_dirname(), "templates")    

def mk_path(path):
    if path and not os.path.exists(path):
        os.makedirs(path)

def mk_path_for_file(f):
    mk_path(os.path.dirname(f))

def get_proxy_from_environment():
    ''' Attempt to get them from "http_proxy" environment variable '''
    protocols = ['http', 'https']
    proxies = {}
    for p in protocols:
        proxy = os.environ.get("%s_proxy" % p)
        if proxy:
            proxies[p] = proxy
    
    return proxies

def setup_console_logger(**kwargs):
    ''' Return a logger defaulting to stderr, INFO, BASIC_FORMAT  '''

    stream = kwargs.get("stream", sys.stderr)
    log_level = kwargs.get("log_level", logging.INFO)
    log_format = kwargs.get("log_format", logging.BASIC_FORMAT)
    name = kwargs.get("name")
    
    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    handler = logging.StreamHandler(stream)
    handler.setFormatter(logging.Formatter(log_format))
    logger.addHandler(handler)
    return logger 