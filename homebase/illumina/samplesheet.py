'''

Code taken from Jinghua (Frank) Feng's bwa_gatk_freebayes_annotate_pipeline

'''

import os

import numpy as np
import pandas as pd


ILLUMINA = 'ILLUMINA'
ILLEGAL_CHARACTERS = "?()[]/\=+<>:;\"',*^|&. " # From Illumina docs
GROUP_COLUMN = "Group"
SAMPLE_PROJECT_COLUMN = "Sample_Project"
SAMPLE_ID_COLUMN = "Sample_ID"
DESCRIPTION_COLUMN = "Description"
LANE_COLUMN = "Lane"
RECIPE_COLUMN = "Recipe"
BIOINFORMATICIAN_COLUMN = "Bioinformatician"
MANDATORY_COLUMNS = [SAMPLE_ID_COLUMN, GROUP_COLUMN, DESCRIPTION_COLUMN]
NO_SPACES_COLUMNS = [SAMPLE_ID_COLUMN, 'Sample_Name']
SAMPLE_NUMBER_COLUMN = "sample_number"

def is_new_sheet(sheet):
    with open(sheet) as f:
        first_line = f.readline().strip()
        return first_line.startswith('[Header]')
    
    return False


def get_sample_numbers(sample_sheet_df, lane_splitting):
    # Same number of samples as entries in the sheet
    if lane_splitting:
        sample_number = pd.Series(range(1, len(sample_sheet_df)+1), index=sample_sheet_df.index)
    else:
        # Sample numbers are combined across lanes - ID is the number unique sample IDs so far
        sample_names = set()
        sample_number = pd.Series(index=sample_sheet_df.index)
        for i, sample in sample_sheet_df[SAMPLE_ID_COLUMN].iteritems():
            sample_names.add(sample)
            sample_number[i] = len(sample_names)
    return sample_number


def samplesheet_is_valid(sample_sheet_df):
    ''' returns is_valid, error message '''
    for column in NO_SPACES_COLUMNS:
        if column in sample_sheet_df.columns:
            col = sample_sheet_df[column].dropna() # Handle crazy NaN spreadsheets (saved as CSV with blank rows/cols)
            
            for row in col:
                for char in row:
                    if char in ILLEGAL_CHARACTERS:
                        msg = "Cannot handle illegal character or whitespace in the sample sheet\n"
                        msg += "Column: %s, row value: '%s', illegal char = '%s' \n" % (column, row, char)
                        return False, msg

    index_column = None
    POSSIBLE_INDEX_COLUMNS = ["Index", "index"]
    for i in POSSIBLE_INDEX_COLUMNS:
        if i in sample_sheet_df.columns:
            index_column = i
            break
        
    if index_column is None:
        msg = "Couldn't find index column in samplesheet, looked for %s" % (','.join(POSSIBLE_INDEX_COLUMNS))
     

    barcode_lengths = sample_sheet_df[index_column].str.len().value_counts()
    if len(barcode_lengths) != 1:
        msg = 'Barcodes must be all the same size, was: %s' % barcode_lengths
        return False, msg
    
    for col in MANDATORY_COLUMNS:
        if col not in sample_sheet_df.columns:
            msg = "SampleSheet.csv is missing '%s' columns" % col
            return False, msg            
    
    return True, None

def get_run_date_and_fcid_from_dir(fc_dir):
    dir_components = os.path.basename(os.path.normpath(fc_dir)).split(os.path.sep)
    run_dir = dir_components[-1]
    #print "dir components = %s" % dir_components

    (run_date, _, _, full_fcid) = run_dir.split('_')
    return run_date, full_fcid[1:]


def load_old_sample_sheet(samplesheet_csv):
    df = pd.read_csv(samplesheet_csv, index_col=None, comment='#', skip_blank_lines=True, skipinitialspace=True)
    is_valid, error_message = samplesheet_is_valid(df)
    if not is_valid:
        raise ValueError(error_message)
    return df

def sections_as_dataframes(csv_file):
    df = pd.read_csv(csv_file, header=None, index_col=None, comment='#', skip_blank_lines=True, skipinitialspace=True, dtype=str)
    df = df.dropna(how='all')
    header_index = df[0].str.startswith("[").fillna(False)

    sections = {}
    previous_start = None
    previous_name = None
    for i, name in df[header_index][0].iteritems():
        name = name[1:-1] # Remove brackets
        start = i + 1
        if previous_start:
            sections[previous_name] = df.loc[previous_start:i-1] 

        previous_name = name
        previous_start = start

    # Rest of df for final section
    if previous_name:
        sections[name] = df.loc[start:] 

    return sections

def sample_sheet_section_as_dict(df):
    section = {}
    df = df.replace({np.NaN : None})
    for _, (a,b) in df[[0,1]].iterrows():
        section[a] = b
    return section

def reads_data_section(df):
    READ_NAMES = ["Read1", "Read2"]
    data = {}
    for read, read_length in zip(READ_NAMES, df[0]):
        data[read] = read_length
    return data


def spreadsheet_data_section(df):
    i = df.index[0]
    columns = df.ix[i]
    df = df.ix[i+1:] # Remove column line
    df.index = range(len(df.index)) # Make index start from 0
    df.columns = columns

    is_valid, error_message = samplesheet_is_valid(df)
    if not is_valid:
        raise ValueError(error_message)

    if LANE_COLUMN in df.columns:
        df.loc[:,LANE_COLUMN] = df[LANE_COLUMN].astype(int)
        
    return df


def load_sample_sheet_as_sections(csv_file):
    SECTION_CONVERTERS = {"Reads" : reads_data_section,
                          "Data" : spreadsheet_data_section}
    orig_sections = sections_as_dataframes(csv_file) 
     
    sections = {}
    for (k, df) in orig_sections.iteritems():
        converter = SECTION_CONVERTERS.get(k, sample_sheet_section_as_dict)
        sections[k] = converter(df)

    return sections
    
def get_trimming_info(settings_section):
    adapter = settings_section.get('Adapter')
    adapter2 = settings_section.get('AdapterRead2')
    
    trimming = adapter or adapter2
    if trimming:
        trimming_message = "Adapters will be trimmed. Adapter: '%s'" % adapter
        if adapter2:
            trimming_message += ", Adapter2: '%s'" % adapter2
    else:
        trimming_message = "No trimming will occur as no adapters specified."
    
    return trimming, trimming_message
    

    
