'''
Created on 23/11/2015

@author: dlawrence
'''

import logging
import os

import xml.etree.ElementTree as ET

class IlluminaSequencer(object):
    MISEQ = 'MiSeq'
    NEXTSEQ = 'NextSeq'
    HISEQ = 'HiSeq'

    SEQUENCERS = [MISEQ, NEXTSEQ, HISEQ]


def get_single_element(run_info_file, root, element_name):
    values = list(root.iter(element_name))
    if len(values) != 1:
        msg = "Not exactly 1  <%s> tag in %s" % (element_name, run_info_file) 
        raise ValueError(msg)
    return values[0]

def get_element_from_xml_file(xml_file, element_id):
    tree = ET.parse(xml_file)
    root = tree.getroot()
    return get_single_element(xml_file, root, element_id)


def get_instrument_from_run_info(flowcell_dir):
    run_info_file = os.path.join(flowcell_dir, "RunInfo.xml")
    return get_element_from_xml_file(run_info_file, "Instrument")

def get_run_parameters_file(flowcell_dir):
    # Damn you Illumina! MiSeq has lower-case 'r'
    RUN_PARAMETERS_FILES = ["RunParameters.xml", "runParameters.xml"]
    run_parameters_file = None
    for f in RUN_PARAMETERS_FILES:
        potential_run_info_file = os.path.join(flowcell_dir, f)
        if os.path.exists(potential_run_info_file):
            run_parameters_file = potential_run_info_file
            break
        
    if run_parameters_file is None:
        msg = "Couldn't find %s in %s" % (" or ".join(RUN_PARAMETERS_FILES), flowcell_dir) 
        raise ValueError(msg)

    return run_parameters_file


def get_run_parameters(flowcell_dir):
    ''' returns sequencer_model, instrument, experiment_name, paired_end '''

    run_parameters_file = get_run_parameters_file(flowcell_dir)
    tree = ET.parse(run_parameters_file)
    root = tree.getroot()

    sequencer_model = None
    try:
        application_name_element = get_single_element(run_parameters_file, root, "ApplicationName")
        application_name = application_name_element.text 
        sequencer = application_name.split()[0]
        if sequencer in IlluminaSequencer.SEQUENCERS:
            sequencer_model = sequencer
        if not sequencer_model:
            possibilities = ','.join(IlluminaSequencer.SEQUENCERS)
            msg = "Couldn't obtain sequencer from ApplicationName (%s). Expected to start with one of %s" % (application_name, possibilities)
            raise ValueError(msg)    
    except:
        pass

    try:
        instrument_element = get_single_element(run_parameters_file, root, "InstrumentID")
    except:
        logging.warning("Couldn't get 'InstrumentID' from RunParameters.xml, trying RunInfo.xml")
        instrument_element = get_instrument_from_run_info(flowcell_dir)
    
    instrument = instrument_element.text
    experiment_name = None
    try:
        experiment = get_single_element(run_parameters_file, root, "ExperimentName")
        experiment_name = experiment.text
    except:
        pass

    read2 = get_single_element(run_parameters_file, root, "Read2")
    paired_end = int(read2.text) != 0  
    return sequencer_model, instrument, experiment_name, paired_end
    
